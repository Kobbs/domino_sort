#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define MAX 200

struct domino {
	int num;
	int size;
	int frak[300];
	struct domino* link;
};

int ai; // array iteration

void generateNumbers(int*, int);
void Domino_Sort(int*, int);
int Nsplit(int*, int, int);
int sortlist(struct domino*, int, int*);
int count(struct domino*);
struct domino* addNode(struct domino*, int);
struct domino* addNode2(struct domino*, int, int, int*);
struct domino* popnode(struct domino*);
void printNumbers(int*, int);
void printFrak(struct domino*);

int main(int argc, char** argv)
{
	static int numbers[2000], size=2000;
	int time;
	generateNumbers(numbers, size);
	Domino_Sort(numbers, size);
	printNumbers(numbers, size);
}

struct domino* addNode(struct domino* domino, int a)
//adds new node to linked list with fractalized number
{
	struct domino* node = calloc(1, sizeof(struct domino));
	int size;

	size = Nsplit(node->frak, a, 0);
	node->num = a;
	node->size = size;
	node->link = domino;
	return node;
}
 
struct domino* addNode2(struct domino* domino, int a, int size, 
				int* frak)
// add node to linked list with values already discovered
{
	struct domino* node = calloc(1, sizeof(struct domino));
	
	node->num = a;
	node->size = size;
	memcpy(node->frak, frak, 300);
	node->link = domino;
	return node;
}


struct domino* popnode(struct domino* domino)
{
	struct domino* node;
	node = domino->link;
	free(domino);
	return node;
}

void Domino_Sort(int* a, int size)
//create linked list with fractilized number, send to sortlist function
//which reassigns values of a[] in low to high order
{
	struct domino* list=NULL;
	int i, time;

	for (i=0; i<size; i++)
		list = addNode(list, a[i]);
	ai=0;
	//time = clock();
	sortlist(list, 0, a); //zeroes for level and array iteration
	//printf("%li\n", clock() - time);
}

int Nsplit(int* a, int num, int level)
//fractalizes num by halving with remainder retention
{
	int halves=0, rem=0;

	while (num>MAX)
	{//loop to break up number (remainder division squarelaw)
		if (num%2)
			rem += (int) pow(2, halves);
		halves++;
		num = num/2;
	}

	if (halves>MAX) { //halves recursion
		a[level++] = MAX + 1; 	           // MAX+1 indicates 
		level = Nsplit (a, halves, level); //value fractalized
	} else
		a[level++] = halves; 
	a[level++] = num;
	
	if (rem>MAX){ //remainder recursion
		a[level++] = MAX + 1;
		return Nsplit(a, rem, level);
	}
	a[level++] = rem;
	return level;
}	

int count(struct domino* list)
//counts elements in linked list
{
	int count = 0;

	while(list)
	{
		count++;
		list = list->link;
	}

	return count;
}

int sortlist(struct domino* list, int level, int* a)
//recursive function to sort linked list into MAX + 2 boxes
{
	struct domino* box[MAX + 2], *traverse;
	int i;

	for(i=0; i<MAX + 2; i++) //set array null
		box[i] = NULL;
	while(list)
	{ //add a node to box chosen by struct array element
		box[list->frak[level]]= addNode2(box[list->frak[level]],
						list->num, list->size,
							list->frak);
		list = popnode(list);
	}

	for (i=0; i<MAX + 2; i++) {
		if (count(box[i]) == 0)
			continue;	
		if (count(box[i]) == 1){ //put single number into array
			a[ai++] = box[i]->num;
			continue;
		}	
		if (box[i]->size == level){
			while(box[i])
			{ //put all equal numbers into array
				a[ai++] = box[i]->num;
				box[i] = box[i]->link;
			}
			continue;
		}
		sortlist(box[i], level + 1, a/*, ai*/);
	}	
}

void printNumbers(int *a, int size)
//print array 8 numbers per line
{
	int i;

	for(i=0; i<size; i++){
		printf("%i \n", a[i]);
	}
}

void generateNumbers(int* a, int size)
//generate array of random numbers
{
	int i;

	srand(clock());
	for(i=0; i<size; i++)
		a[i] = rand() % 10000;
}

void printFrak(struct domino* domino)
{
	int i;

	while(domino)
	{
		printf("%i| ", domino->num);
		for(i=0; i<domino->size; i++)	
			printf("%i : ", domino->frak[i]);
		domino = domino->link;
		printf("\n\n");
	}
}
